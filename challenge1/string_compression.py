from itertools import groupby

def compress_str(str):
  groups = groupby(str)
  result = [(label, sum(1 for _ in group)) for label, group in groups]
  ", ".join("{}{}".format(label, count) for label, count in result)
